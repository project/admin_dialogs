<?php

namespace Drupal\admin_dialogs;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Url;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Component\Serialization\Json;
use Drupal\admin_dialogs\Controller\AdminDialogGroupListBuilder;
use Drupal\admin_dialogs\Controller\AdminDialogListBuilder;
use Drupal\admin_dialogs\Entity\Form\AdminDialogEditForm;
use Drupal\admin_dialogs\Entity\Form\AdminDialogGroupEditForm;

/**
 * Admin Dialogs module.
 */
class AdminDialogsModule {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
 
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * Implements hook_theme().
   */
  public function theme() {
    return [
      'admin_dialog_form' => [
        'render element' => 'form',
      ],
    ];
  }

  /**
   * Implements hook_form_alter().
   */
  public function form_alter(&$form, FormStateInterface $form_state, $form_id) {
    if ($form_state->getFormObject() instanceof EntityFormInterface) {
      $entity = $form_state->getFormObject()->getEntity();
    }
    $config = $this->configFactory->get('admin_dialogs.settings');
    if ($config->get('delete_buttons') && !empty($form['actions']['delete'])) {
      $form['actions']['delete']['#attributes'] = [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 600]),
      ];
    }
    if (!empty($form['actions']['cancel'])) {
      $form['actions']['cancel']['#attributes']['class'] = ['button', 'dialog-cancel'];
      if (!empty($form['description']['#markup'])) {
        $form['description']['#prefix'] = '<p>';
        $form['description']['#suffix'] = '</p>';
      }
    }
  }

  /**
   * Implements hook_entity_operation_alter().
   */
  public function entity_operation_alter(array &$operations, EntityInterface $entity) {
    if (in_array($entity->getEntityTypeId(), ['admin_dialog', 'admin_dialog_group'])) {
      $attributes = [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 600]),
      ];
      if (!empty($operations['delete'])) {
        $operations['delete']['attributes'] = $attributes;
      }
      if (!empty($operations['edit'])) {
        $operations['edit']['attributes'] = $attributes;
      }
    }
    $dialogs = $this->entityTypeManager->getStorage('admin_dialog') ->loadByProperties(['type' => 'ops', 'status' => 1]);
    foreach ($dialogs as $dialog) {
      $criteria = $dialog->getSelectionCriteria();
      if (!empty($criteria['key']) && !empty($operations[$criteria['key']])) {
        if ($this->checkEntityTypeMatch($entity, $criteria)) {
          $operations[$criteria['key']]['attributes'] = $this->getAttributes($dialog);
        }
      }
    }
    $config = $this->configFactory->get('admin_dialogs.settings');
    if ($config->get('delete_ops') && !empty($operations['delete'])) {
      $operations['delete']['attributes'] = [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 600]),
      ];
    }
  }

  /**
   * Implements hook_menu_local_tasks_alter().
   */
  public function menu_local_tasks_alter(&$data, $route_name, RefinableCacheableDependencyInterface &$cacheability) {
    $dialogs = $this->entityTypeManager->getStorage('admin_dialog') ->loadByProperties(['type' => 'tasks', 'status' => 1]);
    foreach ($dialogs as $dialog) {
      $criteria = $dialog->getSelectionCriteria();
      if (!empty($criteria['routes'])) {
        foreach ($criteria['routes'] as $route) {
          foreach ($data as $key => $items) {
            if (!empty($items)) {
              foreach ($items as $index => $link) {
                if (!empty($link[$route])) {
                  $data[$key][$index][$route]['#link']['url']->setOption('attributes', $this->getAttributes($dialog));
                }
              }
            }
          }
        }
      }
    }
  }

  /**
   * Implements hook_menu_local_actions_alter().
   */
  public function menu_local_actions_alter(&$local_actions) {
    $dialogs = $this->entityTypeManager->getStorage('admin_dialog') ->loadByProperties(['type' => 'actions', 'status' => 1]);
    foreach ($dialogs as $dialog) {
      $criteria = $dialog->getSelectionCriteria();
      if (!empty($criteria['routes'])) {
        foreach ($criteria['routes'] as $route) {
          foreach ($local_actions as $action_route => $params) {
            if (strstr($route, '*')) {
              $needle = str_replace('*', '', $route);
              if (strstr($action_route, $needle)) {
                $local_actions[$action_route]['options']['attributes'] = $this->getAttributes($dialog);
              }
            }
            else {
              if ($action_route === $route) {
                $local_actions[$route]['options']['attributes'] = $this->getAttributes($dialog);
              }
            }
          }
        }
      }
    }
  }

  /**
   * Implements hook_entity_type_build().
   */
  public function entity_type_build(array &$entity_types) {
    if (array_key_exists('admin_dialog', $entity_types)) {
      $entity_types['admin_dialog']
        ->set('admin_permission', 'administer dialogs')
        ->setHandlerClass('list_builder', AdminDialogListBuilder::class)
        ->setFormClass('add', AdminDialogEditForm::class)
        ->setFormClass('edit', AdminDialogEditForm::class)
        ->setFormClass('delete', EntityDeleteForm::class)
        ->setLinkTemplate('list-form', '/admin/config/user-interface/dialogs/manage/{admin_dialog_group}/dialogs')
        ->setLinkTemplate('edit-form', '/admin/config/user-interface/dialogs/edit/{admin_dialog}')
        ->setLinkTemplate('delete-form', '/admin/config/user-interface/dialogs/delete/{admin_dialog}');
    }

    if (array_key_exists('admin_dialog_group', $entity_types)) {
      $entity_types['admin_dialog_group']
        ->set('admin_permission', 'administer dialogs')
        ->setHandlerClass('list_builder', AdminDialogGroupListBuilder::class)
        ->setFormClass('add', AdminDialogGroupEditForm::class)
        ->setFormClass('edit', AdminDialogGroupEditForm::class)
        ->setFormClass('delete', EntityDeleteForm::class)
        ->setLinkTemplate('edit-form', '/admin/config/user-interface/dialogs/manage/{admin_dialog_group}')
        ->setLinkTemplate('delete-form', '/admin/config/user-interface/dialogs/manage/{admin_dialog_group}/delete');
    }
  }

  /**
   * Check if entity type and a bundle match criteria.
   */
  protected function checkEntityTypeMatch($entity, $criteria) {
    $matched = FALSE;
    if (!empty($criteria['entity_type'])) {
      if ($entity->getEntityTypeId() == $criteria['entity_type']) {
        $matched = TRUE;
        if (!empty($criteria['bundles'])) {
          $matched = in_array($entity->bundle(), $criteria['bundles']);
        }
      }
    }
    return $matched;
  }

  /**
   * Get dialog attributes.
   */
  protected function getAttributes($dialog) {
    $attributes = [
      'class' => ['use-ajax'],
      'data-dialog-type' => 'modal',
      'data-dialog-options' => Json::encode(['width' => $dialog->getDialogWidth()]),
    ];
    if ($dialog->getDialogType() == 'off_canvas') {
      $attributes['data-dialog-type'] = 'dialog';
      $attributes['data-dialog-renderer'] = 'off_canvas';
    }
    return $attributes;
  }

}
