<?php

namespace Drupal\admin_dialogs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DialogSettingsForm. The config form for the admin_dialogs module.
 *
 * @package admin_dialogs
 */
class AdminDialogSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'admin_dialogs.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_dialogs_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('admin_dialogs.settings');
    $form['delete_ops'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add modal dialog to all "delete" operation links (global).'),
      '#description' => $this->t('This will make Delete operation link open confirmation form in a modal.'),
      '#default_value' => $config->get('delete_ops'),
    ];
    $form['delete_buttons'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add modal dialog to all "delete" action buttons (global).'),
      '#description' => $this->t('This will make Delete action button open confirmation form in a modal.'),
      '#default_value' => $config->get('delete_buttons'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('admin_dialogs.settings')
      ->set('delete_ops', $form_state->getValue('delete_ops'))
      ->set('delete_buttons', $form_state->getValue('delete_buttons'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
